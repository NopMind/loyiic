<?php
namespace app\modules\loyiic\logics\test;

use Yii;
use app\modules\loyiic\lib\ALogic;
use app\modules\loyiic\helpers\FileHelper;

use app\modules\loyiic\logics\test\testLogic;

class indexLogic extends ALogic
{
	public function run()
	{
		# create sub logic stack
		$oLogicStackTwo = new ALogic( $this->oAction, $this->oStore, ALogic::LOGIC_STACK );
		# add an operative logic
		$oLogicStackTwo->addLogic( new testLogic( $this->oAction, $this->oStore ) );

		# create main logic stack
		$oLogicStack = new ALogic( $this->oAction, $this->oStore, ALogic::LOGIC_STACK );
		# add an operative logic
		$oLogicStack->addLogic( new testLogic( $this->oAction, $this->oStore ) );
		# add an operative logic and safe stack id
		$nRID = $oLogicStack->addLogic( new testLogic( $this->oAction, $this->oStore ) );
		# add a conditional logic ( this will be executed because condition is fulfilled)
		$oLogicStack->addLogic( new testLogic( $this->oAction, $this->oStore ), $nRID, time() );
		# add a conditional logic and safe stack id ( this will NOT be executed since condition mismatches)
		$nBID = $oLogicStack->addLogic( new testLogic( $this->oAction, $this->oStore ), $nRID, time() + 5 );
		# add a conditional logic ( this will NOT be executed since related logic didn't execute )
		$oLogicStack->addLogic( new testLogic( $this->oAction, $this->oStore ), $nBID, time() );
		# add a stack logic ( previously generated )
		$oLogicStack->addLogic( $oLogicStackTwo );

		# execute stack logic and save results
		$var = $oLogicStack->execute();
		var_dump($var);

		# get logic result manually by stack id
		$mixBIDResult = $oLogicStack->getResult( $nRID );
		var_dump($mixBIDResult);

		# reset a condition
		$oLogicStack->setCondition( $nBID, time() );
		var_dump($oLogicStack->getCondition($nBID));
		$varTwo = $oLogicStack->execute();
		var_dump($varTwo);

		return "HI";
	}
}