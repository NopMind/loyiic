<?php
namespace app\modules\loyiic\lib;

use Yii;

/**
 *	Logic base class
 *
 */
class ALogic
{
	const LOGIC_STANDALONE = 0;
	const LOGIC_STACK = 1;

	const STACK_LOGIC = 0;
	const STACK_RELATION = 1;
	const STACK_CONDITION = 2;
	const STACK_RESULT = 3;
	const STACK_STATUS = 4;

	const STATUS_BLANK = 0;
	const STATUS_EXECUTED = 1;
	const STATUS_ERROR = 2;

	/// corresponding Action
	public $oAction;
	/// global store
	public $oStore;
	/// type of logic (standalone/stack)
	private $nType;
	/// sub-logics to work as a stack
	public $vStack;

	private $bReset = FALSE;

	public function __construct( $oParentAction, $oStore, $nLogicType = self::LOGIC_STANDALONE, $vStack = [] )
	{
		$this->oAction = $oParentAction;
		$this->oStore = $oStore;
		$this->nType = $nLogicType;
		$this->vStack = $vStack;
	}

	private function setStackVar( $nId, $nKey, $mixValue )
	{
		$this->vStack[$nId][$nKey] = $mixValue;
	}

	public function getStackVar( $nId, $nKey )
	{
		return $this->vStack[$nId][$nKey];
	}

	public function getLogic( $nId )
	{
		return $this->getStackVar( $nId, static::STACK_LOGIC );
	}

	public function getResult( $nId )
	{
		return $this->getStackVar( $nId, static::STACK_RESULT );
	}

	public function getStatus( $nId )
	{
		return $this->getStackVar( $nId, static::STACK_STATUS );
	}

	public function getRelation( $nId )
	{
		return $this->getStackVar( $nId, static::STACK_RELATION );
	}

	public function getCondition( $nId )
	{
		return $this->getStackVar( $nId, static::STACK_CONDITION );
	}

	public function setRelation( $nBaseId, $nRelationId, $mixCondition )
	{
		$this->setStackVar( $nBaseId, static::STACK_RELATION, $nRelationId );
		$this->setStackVar( $nBaseId, static::STACK_CONDITION, $mixCondition );
	}

	public function setCondition( $nBaseId, $mixCondition )
	{
		$this->setStackVar( $nBaseId, static::STACK_CONDITION, $mixCondition );
	}

	public function getResults( )
	{
		$vResults = [];
		foreach( $this->vStack as $nStackId => $vLogic )
			$vResults[$nStackId] = $this->getResult( $nStackId );
		return $vResults;
	}


	public function run() {}

	/**
	 *	Runs a stack logic
	 *
	 *	@return array
	 *	Array of results where [StackID => Result]
	 */
	public function runStack()
	{
		# iterate through stack
		foreach ( $this->vStack as $nStackId => $vLogic )
		{
			# if logic where executed and should not be resetted
			if( ($vLogic[static::STACK_STATUS] !== static::STATUS_BLANK) && !$this->bReset )
				continue;

			if( ($nRelationId = $vLogic[static::STACK_RELATION]) !== NULL )
			{
				# if conditional logic (looks for an relation StackID)
				$this->runConditional( $vLogic, $nRelationId, $nStackId );
			} else {
				# if operative logic
				$this->runOperative( $vLogic, $nStackId );
			}
		}

		# toggle resetting off
		if( $this->bReset )
			$this->bReset = FALSE;

		# catch and return results
		return $this->getResults();
	}

	private function runConditional( $vLogic, $nRelationId, $nStackId )
	{
		# check if related logic has executed and a result
		if( $this->getStatus( $nRelationId ) === static::STATUS_EXECUTED && ($mixResult = $this->getResult( $nRelationId ) ) !== NULL )
		{
			if( $mixResult === $vLogic[static::STACK_CONDITION] )
			{
				$this->setStackVar( $nStackId, static::STACK_RESULT, $vLogic[static::STACK_LOGIC]->execute() );
				$this->setStackVar( $nStackId, static::STACK_STATUS, static::STATUS_EXECUTED );
			} else {
				$this->setStackVar( $nStackId, static::STACK_STATUS, static::STATUS_ERROR );
			}
		} else {
			$this->setStackVar( $nStackId, static::STACK_STATUS, static::STATUS_ERROR );
		}
	}

	private function runOperative( $vLogic, $nStackId )
	{
		$this->setStackVar( $nStackId, static::STACK_RESULT, $vLogic[static::STACK_LOGIC]->execute() );
		$this->setStackVar( $nStackId, static::STACK_STATUS, static::STATUS_EXECUTED );
	}

	public function execute()
	{
		if( $this->nType === static::LOGIC_STANDALONE )
			return $this->run();
		elseif( $this->nType === static::LOGIC_STACK )
		{
			if( isset($this->vStack) )
				$this->bReset = TRUE;

			return $this->runStack();
		}
	}

	public function beforeRun()
	{
		
	}

	public function afterRun() {}

	public function addLogic( $mixLogic, $nRelation = null, $mixCondition = null )
	{
		return array_push( $this->vStack, [
			static::STACK_LOGIC => $mixLogic,
			static::STACK_RELATION => $nRelation,
			static::STACK_CONDITION => $mixCondition,
			static::STACK_STATUS => static::STATUS_BLANK,
			static::STACK_RESULT => NULL
		]) - 1;
	}
}