<?php
namespace app\modules\loyiic\lib;

use Yii;
use yii\base\Controller;

use app\modules\loyiic\lib\CStore;
use app\modules\loyiic\helpers\FileHelper;

abstract class AController extends Controller
{
	const BEFORE_ACTION_HANDLER = 'preAction';
	const AFTER_ACTION_HANDLER = 'postAction';

	const METHOD_CONFIG = '_config';
	const METHOD_ACTIONS = 'actions';

	public $oStore = null;
	protected $vActions = null;

	public function __construct( $mixId, $oModule, $vConfig = [] )
	{
		$this->vActions = $this->loadActions();
		$this->oStore = new CStore( $this );
		$this->registerEventHandlers();

		parent::__construct( $mixId, $oModule, $vConfig );
	}

	public function preAction( $oAction )
	{

	}

	public function postAction( $oAction )
	{
		
	}

	private function registerEventHandlers()
	{
		$this->on( Controller::EVENT_BEFORE_ACTION, [ $this, static::BEFORE_ACTION_HANDLER ] );
        $this->on( Controller::EVENT_AFTER_ACTION, [ $this, static::AFTER_ACTION_HANDLER ] );
	}


	private function loadActions( $vActionConf = null ) 
	{
		$vActions = [];
		

	}

	protected function loadConfig( $sConfigPath = null )
	{
		$vConfig = [];
		if( isset($sConfigPath) )
		{
			if( !file_exists($sConfigPath) )
			{
				# TODO: add error
				return FALSE;
			} else {
				$vConfig = require_once($sConfigPath);
				return $vConfig;
			}
		} else {
			$sMethodName = static::METHOD_CONFIG;
			if( !method_exists( $this, $sMethodName ) )
			{
				# TODO: add error
				return FALSE;
			} else {
				$vConfig = $this->$sMethodName();
				return $vConfig;
			}
		}
	}
}