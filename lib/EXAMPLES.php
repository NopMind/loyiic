<?php

# create and exec standalone logic
$oLogic = new RandomLogic( $oAction, $oStore );
$mixResult = $oLogic->execute();

# create and exec stack logic
$oStackLogic = new ALogic( $oAction, $oStore, ALogic::LOGIC_STACK );
// add operative logic
$oStackLogic->addLogic( new OperativeLogic( $oAction, $oStore ) );
// add conditional logic
$nId = $oStackLogic->addLogic( new OperativeLogic( $oAction, $oStore ) );
$oStackLogic->addLogic( new ConditionalLogic( $oAction, $oStore ), $nId, TRUE );
$oStackLogic->addLogic( new ConditionalLogic( $oAction, $oStore ), $nId, FALSE );
$mixResult = $oStackLogic->execute();