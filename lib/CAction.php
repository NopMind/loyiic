<?php
namespace app\modules\loyiic\lib;

use Yii;
use yii\base\Action;

use app\modules\loyiic\helpers\FileHelper;


class CAction extends Action
{
	/// validator of action
	protected $oValidator = null;
	/// logic of action
	protected $oLogic = null;

	public $oStore = null;

	/**
	 *	Constructor which initializes the action
	 *
	 *	@param nId
	 *	Action ID
	 *
	 *	@param oController
	 *	Controller which have this action
	 *
	 *	@param oValidator
	 *	Validator of Action
	 *
	 *	@param vConfig
	 *	Optional configuration
	 */
	public function __construct( $mixId, $oController, $oValidator = null, $vConfig = [] )
	{
		parent::__construct( $mixId, $oController, $vConfig );
		$this->oStore = $this->controller->oStore;
		$this->oValidator = $oValidator;
		$this->oLogic = $this->createLogicInstance( $this->findLogic() );
	}

	/**
	 *	Run the Action
	 */
	public function run()
	{
		return $this->oLogic->execute();
	}

	/**
	 *	Before running the Action
	 */
	public function beforeRun()
	{
		if( $this->oValidator )
		{
			if( $this->oValidator->validate() )
			{
				if( $this->oLogic )
					return TRUE;
			} else {
				return FALSE;
			}
		} else {
			if( $this->oLogic )
				return TRUE;
			else
			{
				$oLogicPath = $this->findLogic();
			}
		}
	}

	/**
	 *	After running the Action
	 */
	public function afterRun() {}

	/**
	 *	Find the corresponding logic file
	 *
	 *	@return string | bool
	 *	The filepath | file not found
	 */
	public function findLogic()
	{
		if( $oHandle = opendir( FileHelper::getPath( $this->controller, '@pLogics' ) ) )
		{
			while ( ( $sEntry = readdir($oHandle) ) !== FALSE )
			{
				if( $sEntry === $this->id.'.php' )
					return $this->buildPath().'/'.$sEntry;
			}
		}

		return FALSE;
	}

	/**
	 *	Builds the path to logic files for current Action
	 *
	 *	@return string
	 *	Action directory
	 */
	private function buildPath()
	{
		$sLogicPath = Yii::getAlias('@pLogics').'/'.$this->controller->id;
		return $sLogicPath;
	}

	/**
	 *	Creates an instance of the current logic class
	 *
	 *	@param sClassFile
	 *	Filepath to logic class file
	 *
	 *	@return oLogic
	 *	Instance of logic class
	 */
	private function createLogicInstance( $sClassFile )
	{
		$sBasePath =  Yii::getAlias('@app');
		# convert classfilepath to classnamespace
		$sNameSpace = 'app'.str_replace('/', '\\', str_replace( '.php', 'Logic', substr( $sClassFile, strstr( $sClassFile, $sBasePath) + strlen($sBasePath) ) ) );

		require_once $sClassFile;
		$oInstance = new $sNameSpace( $this, $this->oStore );
		return $oInstance;
		/*require_once( $sClassFile );
		return new $sClassName( $this, $this->controller->oStore );

		$sClassName = FileHelper::getInstance( $this->id.'Logic', $sClassFile );
		$inst = new $sClassName();*/
	}

	private static function pathToSpace( $sPath )
	{
		return str_replace( '/', '\\', $sPath );
	}
}