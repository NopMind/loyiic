<?php
namespace app\modules\loyiic;

use Yii;

class loyiic extends yii\base\Module
{
	public function init()
	{
		parent::init();

		$this->setAliases([
			'@nLoyiic' => str_replace( '\\', '/', __NAMESPACE__ ),
			'@pLoyiic' => __DIR__,

			'@nConfig' => '@nLoyiic/config',
			'@nLogics' => '@nLoyiic/logics',
			'@nActions' => '@nLoyiic/actions',
			'@nControllers' => '@nLoyiic/controllers',
			'@nViews' => '@nLoyiic/views',
			'@nHelpers' => '@nLoyiic/helpers',

			'@pConfig' => '@pLoyiic/config',
			'@pMainConfigFile' => '@pConfig/main.php',
			'@pLogics' => '@pLoyiic/logics',
			'@pActions' => '@pLoyiic/actions',
			'@pControllers' => '@pLoyiic/controllers',
			'@pViews' => '@pLoyiic/views',
		]);

		Yii::configure( $this, require( Yii::getAlias('@pMainConfigFile') ) );
	}
	
}