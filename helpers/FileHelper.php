<?php
namespace app\modules\loyiic\helpers;

use Yii;

abstract class FileHelper
{
	const ALIAS_PATH = 'p';
	const ALIAS_SPACE = 'n';

	const LEVEL_CONTROLLER = 'Controller';
	const LEVEL_ACTION = 'Action';
	const LEVEL_VIEW = 'View';

	public static function getPath( $oController, $sAlias = '@pControllers' )
	{
		if( !static::getAliasType( $sAlias ) === static::ALIAS_PATH )
			$sAlias = static::switchAlias( $sAlias );

		$sContPath = Yii::getAlias( $sAlias );

		$sContPath .= '/' . $oController->id;

		return $sContPath;
	}

	public static function getFile( $sFileName, $sDirectory, $bMultiple = FALSE, $bSubdir = FALSE )
	{
		/// if searching for multiple files
		$mixFiles = [];
		/// if searching in sub-directories
		$vSubDirs = [];

		# open directory for reading purpose
		if( $oHandle = opendir( $sDirectory ) )
		{
			# iterate through every entry in directory
			while ( ( $sEntry = readdir($oHandle) ) !== FALSE )
			{
				# filter directory shortcuts
				if( $sEntry === '.' || $sEntry === '..' )
					continue;

				# full path
				$sEntryPath = $sDirectory.'/'.$sEntry;

				# when searching in subdirectories and entry is subdirectory
				if( $bSubdir && is_dir($sEntryPath) )
					array_push( $vSubDirs, $sEntryPath );

				# when file was found
				if( $sEntry === $sFileName )
				{
					if( $bMultiple )
						array_push( $mixFiles, $sEntryPath);
					else
						return $sEntryPath;
				}
			}

			# check all subdirectories for file
			if( $bSubdir )
			{
				foreach ( $vSubDirs as $sSubPath )
				{
					$sPath = static::getFile( $sFileName, $sSubPath );
					# when file was found
					if( $sPath !== FALSE )
					{
						if( $bMultiple )
							array_push( $mixFiles, $sPath );
						else 
							return $sPath;
					}
				}
			}
		}

		if( count($mixFiles) <= 0 )
			return FALSE;

		return $mixFiles;
	}

	public static function getFullClassName( $sClass, $sAlias = '@pLoyiic' )
	{
		if( static::getAliasType( $sAlias ) !== static::ALIAS_PATH )
			$sAlias = static::switchAlias( $sAlias );

		$sPath = Yii::getAlias( $sAlias );
		$sNamespace = Yii::getAlias( static::switchAlias( $sAlias ) ).'\\';

		return static::getFile( $sClass.'.php', $sPath );
	}

	public static function getInstance( $sClass, $sAlias )
	{
		if( !static::getAliasType( $sAlias ) === static::ALIAS_SPACE )
			$sAlias = static::switchAlias( $sAlias );

		require_once( Yii::getAlias( static::switchAlias( $sAlias ) ) );
		return $sClass;
	}

	public static function getPathFromSpace( $sSpaceAlias )	{ return str_replace( static::ALIAS_SPACE, static::ALIAS_PATH, $sSpaceAlias );	}

	public static function getSpaceFromPath( $sPathAlias ) { return str_replace( static::ALIAS_PATH, static::ALIAS_SPACE, $sPathAlias ); }

	public static function getAliasType( $sAlias )
	{
		if( strpos( $sAlias, static::ALIAS_PATH ) === 1 )
			return static::ALIAS_PATH;
		elseif ( strpos( $sAlias, static::ALIAS_SPACE ) === 1 )
			return static::ALIAS_SPACE;
		else
			return FALSE;
	}

	public static function switchAlias( $sAlias )
	{
		if( static::getAliasType( $sAlias ) === static::ALIAS_PATH )
			return static::getSpaceFromPath( $sAlias );
		else
			return static::getPathFromSpace( $sAlias );
	}

	private static function pathToSpace( $sPath ) { return str_replace( '/', '\\', $sPath ); }
	private static function SpaceToPath( $sSpace ) { return str_replace( '\\', '/', $sSpace ); }

	public static function setAlias( $sDir, $mixId, $sLevel )
	{
		$sDir = strrchr( $sDir , '/' );
		$sParentPath = $sLevel.'s';

		$mixId = ucfirst($mixId);

		Yii::setAlias( '@n'.$sLevel.$mixId, '@n'.$sParentPath.'/'.$mixId );
		Yii::setAlias( '@p'.$sLevel.$mixId, '@p'.$sParentPath.$sDir );
	}

	public static function setSubAlias( $sParentAlias, $mixId, $sLevel )
	{

	}
}